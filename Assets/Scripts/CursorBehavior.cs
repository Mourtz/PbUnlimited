﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

sealed class CursorBehavior : MonoBehaviour {

    public Texture2D _crosshair0, _crosshair1,
        _defaultCursor;

    public void RefreshCursor()
    {
        if (GameEngine._isGamePaused)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.SetCursor(_defaultCursor, Vector2.zero, CursorMode.ForceSoftware);
        }
        else
        {
            if (GameEngine._isFiring)
            {
                Cursor.SetCursor(_crosshair1, Vector2.zero, CursorMode.ForceSoftware);
            }
            else
            {
                Cursor.SetCursor(_crosshair0, Vector2.zero, CursorMode.ForceSoftware);
            }
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    private void Awake() {
        Cursor.lockState = CursorLockMode.Locked;
    }
}
