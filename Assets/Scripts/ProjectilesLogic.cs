﻿using UnityEngine;
using System.Collections;

public class ProjectilesLogic : MonoBehaviour {
    //
    private Rigidbody rb;
    private Color _rndColor;

    public int _colorSample=256;
    //
    public int thrust = 300;
    //
    private GameObject _decal;
    //
    private int _maxthrustloss = 50;
    //
    public Vector3 _forceV;

    private void OnEnable() {
        _rndColor = new Color(Random.Range(0, _colorSample) / (_colorSample - 1f), Random.Range(0, _colorSample) / (_colorSample - 1f), Random.Range(0, _colorSample) / (_colorSample - 1f));
        GetComponent<Renderer>().material.color = _rndColor;
    }

    public void Eject() {
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = false;
        rb.AddForce(_forceV * (thrust-Random.Range(0,_maxthrustloss)) );
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "pb_ball")
        {
            GameObject _decal = Instantiate(GameObject.Find("decal"+Random.Range(0,3).ToString()), collision.contacts[0].point, Quaternion.FromToRotation(Vector3.up, collision.contacts[0].normal)) as GameObject;
            _decal.transform.Translate(Vector3.up * 0.005f);
            _decal.GetComponent<Renderer>().material.color = _rndColor;
            Destroy(_decal, 5);
        }

        Destroy(this.gameObject, 5);
    }
}