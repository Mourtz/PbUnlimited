﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeaponController : MonoBehaviour {
    public GameObject _bullet;

    private int _clipAmmo = 30, _totalAmmo = 500;
    private int _CclipAmmo = 30, _CtotalAmmo = 500;

	private float _recoil=1,_reloadTime=2;

	void Start () {
        GameObject.Find("TotalAmmo").GetComponent<Text>().text = _CtotalAmmo.ToString();
        GameObject.Find("CurrentClipAmmo").GetComponent<Text>().text = _CclipAmmo.ToString();
	}

	void Update () {
        if (Input.mousePresent && GetComponentInParent<NetworkView>().isMine)
        {
			if(Input.GetMouseButton(0)){
                if(_CclipAmmo>0&&!GameEngine._isReloading){
                    GameEngine._isFiring = true;
                    GetComponentInParent<CursorBehavior>().RefreshCursor();
                    
                    _CclipAmmo--;
                    GameObject.Find("CurrentClipAmmo").GetComponent<Text>().text = _CclipAmmo.ToString() ;

                    GameObject _blt = Instantiate(_bullet,_bullet.transform.position,new Quaternion(0,0,0,0))as GameObject;
                    ProjectilesLogic _lg = _blt.GetComponent<ProjectilesLogic>();
                    _lg._forceV = GameObject.Find("FirstPersonCharacter").transform.forward;
                    _blt.SetActive(true);
                    _lg.Eject();
                }
                else if (_CtotalAmmo > 0 && !IsInvoking("Reload"))
                {
                    GameEngine._isFiring = false;
                    GetComponentInParent<CursorBehavior>().RefreshCursor();
                    GameEngine._isReloading = true;
                    Invoke("Reload", _reloadTime);
                }
			}

            if (Input.GetMouseButtonUp(0)) {
                GameEngine._isFiring = false;
                GetComponentInParent<CursorBehavior>().RefreshCursor();
            }

			if(Input.GetMouseButton(1)){
				
				
			}

		}
	}

    void Reload() {
        if (_CtotalAmmo > _clipAmmo)
        {
            _CtotalAmmo -= _clipAmmo;
            GameObject.Find("TotalAmmo").GetComponent<Text>().text = _CtotalAmmo.ToString();
            _CclipAmmo = _clipAmmo;
            GameObject.Find("CurrentClipAmmo").GetComponent<Text>().text = _CclipAmmo.ToString();
        }
        else
        {           
            _CclipAmmo = _CtotalAmmo;
            GameObject.Find("CurrentClipAmmo").GetComponent<Text>().text = _CclipAmmo.ToString();
            _CtotalAmmo = 0;
            GameObject.Find("TotalAmmo").GetComponent<Text>().text = _CtotalAmmo.ToString();
        }
        GameEngine._isReloading = false;
    }
}
